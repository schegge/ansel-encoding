package de.schegge.util.ansel;

import static java.util.stream.IntStream.concat;
import static java.util.stream.IntStream.rangeClosed;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AnselCharsetTest {
	private Charset charset;

	@BeforeEach
	public void setUp() {
		charset = new AnselCharset();
	}

	@Test
	void testNameAndAlias() {
		Assertions.assertEquals("ANSEL", charset.name());
		Assertions.assertEquals(Collections.singleton("ISO-IR-231"), charset.aliases());
	}

	@Test
	void tesContains() {
		Assertions.assertTrue(charset.contains(charset));
		Assertions.assertFalse(charset.contains(java.nio.charset.StandardCharsets.UTF_8));
	}

	@Test
	void testDecodeASCII() {
		ByteBuffer in = ByteBuffer.allocate(128);
		for (int i = 0; i < 128; i++) {
			in.put((byte) i);
		}
		in.position(0);
		CharBuffer out = CharBuffer.allocate(128);
		charset.newDecoder().decode(in, out, true);
		for (int i = 0; i < 128; i++) {
			Assertions.assertEquals((char) i, out.get(i));
		}
	}

	@Test
	void testDecodeSpecialChars() {
		ByteBuffer in = ByteBuffer.allocate(14);
		for (int i = 0xA1; i < 0xAF; i++) {
			in.put((byte) i);
		}
		in.position(0);
		CharBuffer out = CharBuffer.allocate(14);
		charset.newDecoder().decode(in, out, true);
		out.position(0);
		Assertions.assertEquals("ŁØĐÞÆŒʹ·♭®±ƠƯʼ", out.toString());
	}

	@Test
	void testDecodeIllegalChars() {
		IntStream illegalByteCodes = concat(IntStream.of(0xAF, 0xBB, 0xBE, 0xBF, 0xFC, 0xFD, 0xFF),
				concat(rangeClosed(0xC7, 0xDF), rangeClosed(0x80, 0xA0)));
		illegalByteCodes.mapToObj(i -> ByteBuffer.wrap(new byte[] { 0x20, (byte) i, 0x20 })).forEach(x -> {
			CharBuffer out = CharBuffer.allocate(3);
			charset.newDecoder().decode(x, out, true);
			out.position(0);
			Assertions.assertArrayEquals(new char[] { 32, 0, 0, }, out.array());
		});
	}

	@Test
	void testWithInputStreamReader() throws IOException {
		byte[] bytes = new byte[14];
		for (int j = 0, i = 0xA1; i < 0xAF; j++, i++) {
			bytes[j] = (byte) i;
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bytes), charset));
		Assertions.assertEquals("ŁØĐÞÆŒʹ·♭®±ƠƯʼ", reader.readLine());
	}
}
