package de.schegge.util.ansel;

import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.Arrays;

public abstract class AbstractAnselCharset extends Charset {
	protected char[] highTable;

	public AbstractAnselCharset(String canonicalName, String[] aliases, char[] highTable) {
		super(canonicalName, aliases);
		this.highTable = Arrays.copyOf(highTable, highTable.length);
	}

	@Override
	public CharsetDecoder newDecoder() {
		return new AnselCharsetDecoder(this, highTable);
	}

	@Override
	public CharsetEncoder newEncoder() {
		throw new UnsupportedOperationException("we don't like to write GEDCOM");
	}
}