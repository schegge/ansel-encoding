package de.schegge.util.ansel;

import java.nio.charset.Charset;
import java.nio.charset.spi.CharsetProvider;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class AnselCharsetProvider extends CharsetProvider {
	private List<Charset> charsets = Arrays.asList(new AnselCharset(), new GedcomCharset(), new Marc21Charset());

	@Override
	public Iterator<Charset> charsets() {
		return charsets.iterator();
	}

	@Override
	public Charset charsetForName(String charsetName) {
		return charsets.stream().filter(c -> c.name().equals(charsetName)).findFirst().orElse(null);
	}
}
