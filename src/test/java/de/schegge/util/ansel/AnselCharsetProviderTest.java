package de.schegge.util.ansel;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AnselCharsetProviderTest {
	@Test
	void test() {
		AnselCharsetProvider provider = new AnselCharsetProvider();
		Assertions.assertAll(
				() -> assertNotNull(provider.charsetForName("ANSEL")),
				() -> assertNotNull(provider.charsetForName("GEDCOM")),
				() -> assertNotNull(provider.charsetForName("MARC21")));
		
		List<Charset> target = new ArrayList<>();
		provider.charsets().forEachRemaining(target::add);
		Assertions.assertAll(
				() -> assertTrue(target.contains(provider.charsetForName("ANSEL"))),
				() -> assertTrue(target.contains(provider.charsetForName("GEDCOM"))),
				() -> assertTrue(target.contains(provider.charsetForName("MARC21"))));
	}

}
