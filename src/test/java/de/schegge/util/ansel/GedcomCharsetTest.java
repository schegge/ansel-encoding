package de.schegge.util.ansel;

import static java.util.stream.IntStream.concat;
import static java.util.stream.IntStream.rangeClosed;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class GedcomCharsetTest {
	private Charset charset;

	@BeforeEach
	public void setUp() {
		charset = new GedcomCharset();
	}

	@Test
	void testNameAndAlias() {
		assertEquals("GEDCOM", charset.name());
		assertEquals(Collections.emptySet(), charset.aliases());
	}

	@Test
	void tesContains() {
		assertTrue(charset.contains(charset));
		assertTrue(charset.contains(new AnselCharset()));
		assertTrue(charset.contains(StandardCharsets.US_ASCII));
		assertFalse(charset.contains(StandardCharsets.UTF_8));
	}

	@Test
	void testDecodeASCII() {
		ByteBuffer in = ByteBuffer.allocate(128);
		for (int i = 0; i < 128; i++) {
			in.put((byte) i);
		}
		in.position(0);
		CharBuffer out = CharBuffer.allocate(128);
		charset.newDecoder().decode(in, out, true);
		for (int i = 0; i < 128; i++) {
			assertEquals((char) i, out.get(i));
		}
	}

	@Test
	void testDecodeSpecialChars() {
		ByteBuffer in = ByteBuffer.allocate(14);
		for (int i = 0xA1; i < 0xAF; i++) {
			in.put((byte) i);
		}
		in.position(0);
		CharBuffer out = CharBuffer.allocate(14);
		charset.newDecoder().decode(in, out, true);
		out.position(0);
		assertEquals("ŁØĐÞÆŒʹ·♭®±ƠƯʼ", out.toString());
	}

	@Test
	void testDecodeGedcomSpecials() {
		ByteBuffer in = ByteBuffer.allocate(6);
		for (int i : Arrays.asList(0xBE, 0xBF, 0xCD, 0xCE, 0xCF, 0xFC)) {
			in.put((byte) i);
		}
		in.position(0);
		CharBuffer out = CharBuffer.allocate(6);
		charset.newDecoder().decode(in, out, true);
		out.position(0);
		assertEquals("□■eoß\u0338", out.toString());
	}

	@Test
	@Disabled
	void testDecodeIllegalChars() {
		IntStream illegalByteCodes = concat(IntStream.of(0xAF, 0xBB, 0xFD, 0xFF),
				concat(rangeClosed(0xC7, 0xDF), rangeClosed(0x80, 0xA0)));
		illegalByteCodes.filter(x -> x != 0xCE && x != 0xCF)
				.mapToObj(i -> ByteBuffer.wrap(new byte[] { 0x20, (byte) i, 0x20 })).forEach(x -> {
					CharBuffer out = CharBuffer.allocate(3);
					charset.newDecoder().decode(x, out, true);
					out.position(0);
					assertArrayEquals(new char[] { 32, 0, 0, }, out.array());
				});
	}

	@Test
	void testWithInputStreamReader() throws IOException {
		byte[] bytes = new byte[14];
		for (int j = 0, i = 0xA1; i < 0xAF; j++, i++) {
			bytes[j] = (byte) i;
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bytes), charset));
		assertEquals("ŁØĐÞÆŒʹ·♭®±ƠƯʼ", reader.readLine());
	}
}
