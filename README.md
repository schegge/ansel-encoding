# ansel-encoding

Java Character Encoding for [ANSEL](https://en.wikipedia.org/wiki/ANSEL) aka ISO-IR 231, and the derived Character Encodings GEDCOM and MARC21. 
The code is part of a [blog article about custom Java Charset implementations](https://schegge.de/2019/04/03/du-sprechen-ansel).

## Usage

> **Example**
> ```java
> GedComReader reader = new GedComReader();
> GedCom gedcom1 = reader.read(new InputStreamReader(getClass().getResourceAsStream("base.ged"), "ANSEL"));
>
> Charset gedcom = Charset.forName("GEDCOM");
> GedCom gedcom2 = reader.read(new InputStreamReader(getClass().getResourceAsStream("base.ged"), gedcom));
> ```
